# ukljucivanje biblioteke pygame
import pygame
 
pygame.init()
pygame.font.init()


# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900

RED = [ 255, 0, 0 ]

# tuple velicine prozora
size = (WIDTH, HEIGHT)

screen = pygame.display.set_mode(size)

pygame.display.set_caption("Projekt")

bg1=pygame.image.load("bg1.jpg")
bg1=pygame.transform.scale(bg1,(WIDTH,HEIGHT))
bg2=pygame.image.load("bg2.jpg")
bg2=pygame.transform.scale(bg2,(WIDTH,HEIGHT))
bg3=pygame.image.load("bg3.jpg")
bg3=pygame.transform.scale(bg3,(WIDTH,HEIGHT))
bg4=pygame.image.load("bg4.jpg")
bg4=pygame.transform.scale(bg4,(WIDTH,HEIGHT))
bg5=pygame.image.load("bg5.jpg")
bg5=pygame.transform.scale(bg5,(WIDTH,HEIGHT))

myfont= pygame.font.SysFont('Arial',40)
bg1_text=myfont.render("SLAJD 1",False,RED)
bg2_text=myfont.render("SLAJD 2",False,RED)
bg3_text=myfont.render("SLAJD 3",False,RED)
bg4_text=myfont.render("SLAJD 4",False,RED)
bg5_text=myfont.render("SLAJD 5",False,RED)

state = "prvislajd" 
clock = pygame.time.Clock()
done = False
duration = 1000
time = 0

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    if state=="prvislajd":
        duration= duration - time
        if duration <= 0:
            duration = 1000
            state = "drugislajd"

    if state=="drugislajd":
        duration= duration - time
        if duration <= 0:
            duration = 1000
            state = "trecislajd"

    if state=="trecislajd":
        duration= duration - time
        if duration <= 0:
            duration = 1000
            state = "cetvrtislajd"

    if state=="cetvrtislajd":
        duration= duration - time
        if duration <= 0:
            duration = 1000
            state = "petislajd"

    if state=="petislajd":
        duration= duration - time
        if duration <= 0:
            duration = 1000
            state = "prvislajd"


    if state== "prvislajd":
        screen.blit(bg1,(0,0))
        screen.blit(bg1_text,(30,30))

    if state== "drugislajd":
        screen.blit(bg2,(0,0))
        screen.blit(bg2_text,(30,30))

    if state== "trecislajd":
        screen.blit(bg3,(0,0))
        screen.blit(bg3_text,(30,30))

    if state== "cetvrtislajd":
        screen.blit(bg4,(0,0))
        screen.blit(bg4_text,(30,30))

    if state== "petislajd":
        screen.blit(bg5,(0,0))
        screen.blit(bg5_text,(30,30))  

    pygame.display.flip()
   
   
    time = clock.tick(60)
pygame.quit()